<!-- Title for post feed -->
<div class="row">
    <div class="col-sm-12">
        <h3>{{ __('Post Feed') }}</h3>
    </div>
</div>

<!-- Lists all the posts that have been made - default is order by most recent -->
@forelse ($posts as $post)

    <!-- Each post is displayed inside a bootstrap card -->
    <div class="card item-card" id="{{ $post->id }}">

        <!-- Displays information about the post -->
        <div class="card-body">

            <!-- Adds an edit button, if the current user owns this post -->
            @if (Auth::user()->id == $post->user->id)

                <!-- Creates the edit button -->
                <button type="button" class="btn edit-button post-edit-button" data-toggle="modal" data-target="#modal_{{$post->id}}">
                    <img src="{{ URL::asset('/images/general/edit_icon.png') }}" class="d-inline-block" alt="">
                </button>

                <!-- Creates the delete button -->
                <button type="button" class="btn edit-button post-delete-button"
                onclick="deletePost('{{ route ('api.posts.delete') }}', '{{ $post->id }}', '{{ Auth::user()->api_token }}')">
                    <img src="{{ URL::asset('/images/general/delete_icon.png') }}" class="d-inline-block" alt="">
                </button>

                <!-- The Modal to go alongside the comment -->
                @include('components.modals.editpost')
            @endif

            <!-- User Information -->
            <a href="{{ route('users.show', ['id' => $post->user->id]) }}">
                <h5 class="card-title post-owner">{{$post->user->firstname}} {{$post->user->surname}}</h5>
            </a>

            <!-- When the post was created -->
            <h6 class="card-subtitle mb-2 text-muted">{{ date('d-m-Y H:i:s', strtotime($post->created_at)) }}</h6>

            <!-- Card image - if there is one -->
            @if ($post->hasImage())
                <img class="post-image" src="{{ Storage::url($post->image->image_path) }}" />
            @endif

            <!-- Card Contents -->
            <h5 class="card-title post-title">{{ $post->title }}</h5>
            <p class="card-text item-card-text post-content">{{ $post->content }}</p>
        </div>

        <!-- Button to view Comments -->
        <button type="button" onclick="changeCommentsContainer('{{ $post->id }}');" 
        class="btn btn-lg btn-block comment-button">Comments ({{count($post->comments)}})</button>

        <!-- Comments Container -->
        <div class="comment-outer-container">

            <!-- Displays all comments -->
            @foreach ($post->comments as $comment)
            <div class="comment-container" id="comment-container-{{ $comment->id }}">

                    <!-- Provides the option to edit a comment, if the user owns it -->
                    <!-- Edit comment button doesnt appear for newly created comments -->
                    @if (Auth::user()->id == $comment->user->id)

                        <!-- Creates the edit button -->
                        <button type="button" class="btn edit-button" data-toggle="modal" data-target="#modal_{{$comment->id}}">
                            <img src="{{ URL::asset('/images/general/edit_icon.png') }}" class="d-inline-block" alt="">
                        </button>

                        <!-- The Modal to go alongside the comment -->
                        @include('components.modals.editcomment')
                    @endif

                    <!-- The rest of the comment information -->
                    <p class="comment-user">{{ $comment->user->firstname }} {{ $comment->user->surname }}:</p>
                    <p class="comment-content">{{ $comment->content }}</p>
                </div>
            @endforeach
        </div>

        <!-- A container to add a comment to the post -->
        <div id="make-comment-container-{{ $post->id }}" class="comment-container">
            <!-- The comment input text -->
            <input type="text" class="form-control comment-textbox form-control-sm" name="comment"
            id="comment" placeholder="Start writing here..." required>

            <!-- The comment submit button -->
            <button type="button" onclick="makeNewComment('{{ route ('api.comments.store') }}', '{{ $post->id }}',
            '{{ Auth::user()->id }}', '{{ Auth::user()->firstname }}', '{{ Auth::user()->surname }}', '{{count($post->comments)}}',
            '{{ Auth::user()->api_token }}');"
            class="btn btn-sm comment-button comment-submit">{{ __('Make Comment') }}</button>
        </div>
    </div>

<!-- This HTML is displayed if there is no posts -->
@empty

    <!-- An is empty container -->
    <h2 class="is-empty-title">No Posts Found!</h2>

@endforelse