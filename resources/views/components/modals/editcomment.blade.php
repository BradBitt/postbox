<!-- The Modal to go alongside the comment -->
<div class="modal fade" id="modal_{{$comment->id}}">
    <div class="modal-dialog">
        <div class="modal-content">
    
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">{{ __('Edit Comment') }}</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
    
            <!-- Modal body -->
            <div class="modal-body">
                <!-- Edit comment content -->
                <textarea id="edit-comment-content-{{ $comment->id }}" class="form-control form-control-sm" rows="3"
                placeholder="Comment Content...">{{ $comment->content }}</textarea>
            </div>
    
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"
                onclick="editComment('{{ route ('api.comments.update') }}', '{{ $post->id }}', '{{ Auth::user()->id }}',
                '{{ $comment->id }}', '{{ Auth::user()->api_token }}');">Update</button>
            </div>
        </div>
    </div>
</div>