<!-- The Modal to go alongside the post -->
<div class="modal fade" id="modal_{{$post->id}}">
    <div class="modal-dialog">
        <div class="modal-content">
    
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">{{ __('Edit Post') }}</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <!-- Edit post title -->
                <input id="edit-post-title-{{ $post->id }}" class="form-control form-control-sm" value="{{ $post->title }}"
                type="text" placeholder="Post Title...">
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <!-- Edit post content -->
                <textarea id="edit-post-content-{{ $post->id }}" class="form-control form-control-sm" rows="3"
                placeholder="Post Content...">{{ $post->content }}</textarea>
            </div>
    
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"
                onclick="editPost('{{ route ('api.posts.update') }}', '{{ Auth::user()->id }}',
                '{{ $post->id }}', '{{ Auth::user()->api_token }}');">Update</button>
            </div>
        </div>
    </div>
</div>