<!-- The Modal to go alongside the user -->
<div class="modal fade" id="modal-edit-account">
        <div class="modal-dialog">
            <div class="modal-content">
        
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">{{ __('Edit Account') }}</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
    
                <!-- Modal body -->
                <div class="modal-body">
                    <!-- Edit user firstname -->
                    <input id="edit-account-firstname" class="form-control form-control-sm" value="{{ Auth::user()->firstname }}"
                    type="text" placeholder="Firstname...">
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <!-- Edit user surname -->
                    <input id="edit-account-surname" class="form-control form-control-sm" value="{{ Auth::user()->surname }}"
                    type="text" placeholder="Surname...">
                </div>
        
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"
                    onclick="updateAccount('{{ route ('api.account.update') }}', '{{ Auth::user()->id }}', '{{ Auth::user()->api_token }}');">Update</button>
                </div>
            </div>
        </div>
    </div>