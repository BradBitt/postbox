<!-- The Modal to go alongside the create a post button -->
<div class="modal fade" id="create_post_modal">
    <div class="modal-dialog">
        <div class="modal-content">
    
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">{{ __('Create Post') }}</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
    
            <!-- Modal body -->
            <div class="modal-body">
                <!-- Add post title -->
                <input id="new-post-title" class="form-control form-control-sm" type="text" placeholder="Post Title...">
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <!-- Add post content -->
                <textarea id="new-post-content" class="form-control form-control-sm" rows="3" placeholder="Post Content..."></textarea>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <!-- Add post image -->
                <label for="new-post-file">[Optional] Upload an Image:</label>
                <input type="file" class="form-control-file" id="new-post-file">
            </div>
    
            <!-- Modal footer -->
            <!-- Contains the create post button -->
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"
                onclick="makeNewPost('{{ route ('api.posts.store') }}', '{{ Auth::user()->id }}', '{{ Auth::user()->api_token }}');">Post</button>
            </div>
        </div>
    </div>
</div>