@extends('layouts.main')

<!-- Applys HTML to this inherited section -->
@section('content')

    <!-- NavBar -->
    <nav class="navbar navbar-light bg-light fixed-top">

        <!-- Navbar icon -->
        <a class="navbar-brand" href="{{ route('home') }}">
            <img src="{{ URL::asset('/images/logo/postbox_logo.png') }}" width="30" height="30" class="d-inline-block align-top" alt="">
            PostBox
        </a>

        <ul class="navbar-nav mr-auto"></ul>

        <!-- User Profile Button -->
        <a class="navbar-left-button" href="{{ route('account') }}" 
        data-toggle="tooltip" data-placement="bottom" title="My Account">
            <img src="{{ URL::asset('/images/navbar/user-profile-button.png') }}" width="30" height="30" class="d-inline-block align-top" alt="">
        </a>

        <!-- Log out button -->
        <a class="navbar-left-button" href="{{ route('logout') }}" onclick="event.preventDefault(); 
        document.getElementById('logout-form').submit();" data-toggle="tooltip" data-placement="bottom" title="Log Out">
            <img src="{{ URL::asset('/images/navbar/log-off-button.png') }}" width="30" height="30" class="d-inline-block align-top" alt="">
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
    </nav>

    <!-- Yeilds the main content for other pages to use -->
    <div id="app">
        @yield('main-content')
    </div>

    <!-- Footer -->

@endsection