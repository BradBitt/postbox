<!-- This view is public -->
<!-- This is the login screen -->
@extends('layouts.main')

<!-- Applys HTML to this inherited section -->
@section('content')
    <!-- Sign in container -->
    <form method="POST" class="form-signin" action="{{ route('login') }}">

        <!-- The website logo -->
        <img src="{{ URL::asset('/images/logo/postbox_logo.png') }}" id="logo-image" alt="">

        <!-- The csrf-token from the meta-data in head -->
        @csrf

        <!-- Input email -->
        <label for="inputEmail" class="sr-only">{{ __('E-Mail Address') }}</label>
        <input type="email" id="inputEmail" class="form-control @error('email') is-invalid @enderror" placeholder="Email address" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

        <!-- Input password -->
        <label for="inputPassword" class="sr-only">{{ __('Password') }}</label>
        <input type="password" id="inputPassword" class="form-control @error('password') is-invalid @enderror" placeholder="Password" name="password" required autocomplete="current-password">

        <!-- Remember me check box -->
        <div class="checkbox mb-3">
            <div class="form-check">
                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                <label class="form-check-label" for="remember">
                    {{ __('Remember Me') }}
                </label>
            </div>
        </div>

        <!-- Sign in button -->
        <button class="btn btn-lg btn-primary btn-block" type="submit">{{ __('Sign In') }}</button>

        <!-- Login with google -->
        <a class="anchor-container btn btn-link google-login-button" href="{{ route('google.redirect') }}" >
            Login With Google
        </a>

        <!-- Error Section -->
        @error('email')
            <p class="invalid-feedback" role="alert">{{ $message }}</p>
        @enderror
        @error('password')
            <p class="invalid-feedback" role="alert">{{ $message }}</p>
        @enderror
    </form>
@endsection
