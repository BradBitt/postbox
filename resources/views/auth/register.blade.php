<!-- This view is public -->
<!-- This is the register screen -->
@extends('layouts.main')

<!-- Applys HTML to this inherited section -->
@section('content')
    <!-- Register User form -->
    <form method="POST" class="checkmate-form" action="{{ route('register') }}">

        @csrf

        <!-- Title -->
        <h2 id="register-title">Register</h2>

        <!-- Email row -->
        <div class="form-group row">
            <div class="col-sm-12">
                <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="email" placeholder="Email" required autofocus>
                @error('email')
                <div class="invalid-feedback" role="alert">
                    {{ $message }}
                </div>
                @enderror
            </div>
        </div>

        <!-- First Name -->
        <div class="form-group row">
            <div class="col-sm-12">
                <input type="text" class="form-control @error('firstname') is-invalid @enderror" name="firstname" id="firstname" placeholder="First name" required autofocus>
            
                @error('firstname')
                <div class="invalid-feedback" role="alert">
                    {{ $message }}
                </div>
                @enderror
            </div>
            
        </div>

        <!-- Last Name row -->
        <div class="form-group row">
            <div class="col-sm-12">
                <input type="text" class="form-control @error('surname') is-invalid @enderror" name="surname" id="surname" placeholder="Surname" required autofocus>
                
                @error('surname')
                <div class="invalid-feedback" role="alert">
                    {{ $message }}
                </div>
                @enderror
            </div>
        </div>

        <!-- Password box -->
        <div class="form-group row">
            <div class="col-sm-12">
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Password" required autocomplete="new-password">

                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>

        <!-- Confirm password -->
        <div class="form-group row">
            <div class="col-sm-12">
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" required autocomplete="new-password">
            </div>
        </div>

        <!-- Register Button -->
        <div class="form-group row">
            <div class="col-sm-12">
                <button type="submit" class="btn btn-primary">{{ __('Register') }}</button>
            </div>
        </div>
    </form>
@endsection