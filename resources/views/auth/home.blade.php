<!-- This view is the home page for a user logged in -->
@extends('layouts.navfooter')

<!-- Applys HTML to this inherited section -->
@section('main-content')

    <!-- Create a post container -->
    <div class="home-top-container row">

        <!-- Create a post heading -->
        <div class="col-sm-12">
            <h3>Welcome {{ Auth::user()->firstname }}!</h3>
        </div>
        
        <!-- Home Page Content -->
        <div class="col-sm-6">
            <p>
                This is the homepage for the 'PostBox' web application. Here you can 
                view all the posts on the website in most recent order. 
                You have the ability to comment on any post and create a post of your own. 
                Don't worry if you make a mistake when publishing, 
                you can edit any post or comment you have made at any time.
            </p>
        </div>

        <!-- Create a post button -->
        <div class="col-sm-6">
            <button type="button" class="btn btn-secondary btn-lg create-post-button"
            data-toggle="modal" data-target="#create_post_modal">Create A Post</button>
        </div>

        <!-- The Modal to go alongside the create a post button -->
        @include('components.modals.createpost')
    </div>

    <!-- Lists all the posts - using the 'posts' parameter provided -->
    @include('components.postcontainer')

    <!-- Lists all the pages of the pagination -->
    {{ $posts->links() }}
@endsection