<!-- Shows the account of the user provided. -->
<!-- This is not used to view the current users account -->
@extends('layouts.navfooter')

<!-- Applys HTML to this inherited section -->
@section('main-content')

    <!-- A subscribe button -->
    <!-- This shows that the user is currently subscribed to this person, or allows them to subscribe -->
    <div class="user-info-container">
        <h2>{{ $user->firstname }} {{ $user->surname }}</h2>

        <!-- A subscribe button -->
        <!-- If the user is already subscribed then show an unsubscribe button -->
        @if($user->isSubscribed(Auth::user(), $user))
            <button type="button" class="btn btn-danger" id="subscribe-button"
            onclick="toggleSubscribe('{{ route('api.users.subscribe') }}', '{{ route('api.users.unsubscribe') }}',
            '{{ Auth::user()->id }}', '{{ $user->id }}', '{{ Auth::user()->api_token }}')">UnSubscribe</button>

        <!-- if the user is not subscribed then show a subscribe button -->
        @else
            <button type="button" class="btn btn-danger" id="subscribe-button"
            onclick="toggleSubscribe('{{ route('api.users.subscribe') }}', '{{ route('api.users.unsubscribe') }}',
            '{{ Auth::user()->id }}', '{{ $user->id }}', '{{ Auth::user()->api_token }}')">Subscribe</button>
        @endif
    </div>
    

    <!-- Lists all the posts - using the 'posts' parameter provided -->
    @include('components.postcontainer')
    
@endsection