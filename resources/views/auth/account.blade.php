<!-- The users account view -->
@extends('layouts.navfooter')

<!-- Applys HTML to this inherited section -->
@section('main-content')

    <!-- User Information Container -->
    <div class="row account-info-container">
    
        <!-- User Information -->
        <div class="col-sm-6">
            <h3>User Information</h3>
            <p id="firstname-surname-container">Name: {{ Auth::user()->firstname }} {{ Auth::user()->surname }}</p>
            <p id="email-container">Email: {{ Auth::user()->email }}</p>
        </div>

        <!-- Users Subscribers -->
        <div class="col-sm-6 subscription-container">
            <h3>My Subscriptions</h3>
            <div class="subscription-list-container">
                <ul>
                    @foreach (Auth::user()->subscribedTo as $subscribed_to_user)
                        <li>
                            <a href="{{ route('users.show', ['id' => $subscribed_to_user->id]) }}">
                                {{ $subscribed_to_user->firstname }} {{ $subscribed_to_user->surname }}
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>

        <!-- Update user info -->
        <div class="col-sm-12">

            <button type="button" class="btn btn-danger edit-account-button" data-toggle="modal"
            data-target="#modal-edit-account">Edit</button>

            <!-- The Modal to go alongside the user info -->
            @include('components.modals.editaccount')
        </div>
    </div>

    <!-- Lists all the posts - using the 'posts' parameter provided -->
    @include('components.postcontainer')
@endsection