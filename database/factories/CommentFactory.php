<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(App\Comment::class, function (Faker $faker) {
    // The post object this comment will relate to
    $post = App\Post::inRandomOrder()->first();

    return [
        'content' => $faker->sentence($nbWords = 10, $variableNbWords = true),
        'user_id' => App\User::inRandomOrder()->first()->id,
        'post_id' => $post->id
    ];
});
