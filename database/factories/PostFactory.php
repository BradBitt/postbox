<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(App\Post::class, function (Faker $faker) {
    return [
        'title' => $faker->word(),
        'content' => $faker->sentence($nbWords = 10, $variableNbWords = true),
        'user_id' => App\User::inRandomOrder()->first()->id
    ];
});
