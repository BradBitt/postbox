<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(App\PostImage::class, function (Faker $faker) {

    // The post id this PostImage will link to.
    $post_id = App\Post::inRandomOrder()->first()->id;

    // Chooses a random image from the 'test-images' folder.
    $all_test_images = ['test-image-1', 'test-image-2', 'test-image-3', 'test-image-4', 'test-image-5', 'test-image-6'];
    $rand_index = array_rand($all_test_images);

    return [
        'post_id' => $post_id,
        'image_path' => 'test-images/' . $all_test_images[$rand_index] . '.jpg',
    ];
});