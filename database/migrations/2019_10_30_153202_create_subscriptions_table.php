<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * This table creates a many-to-many relationship
 * with the 'users' table. It only contains the user model.
 */
class CreateSubscriptionsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('subscriptions', function (Blueprint $table) {

            // Main Schema
            $table->unsignedBigInteger('subscriber_user_id');
            $table->unsignedBigInteger('subscribed_to_user_id');

            // Keys
            $table->primary(['subscriber_user_id', 'subscribed_to_user_id']);
            $table->foreign('subscriber_user_id')->references('id')->
                on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('subscribed_to_user_id')->references('id')->
                on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('subscriptions');
    }
}