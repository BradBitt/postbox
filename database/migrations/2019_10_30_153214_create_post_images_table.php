<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostImagesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('post_images', function (Blueprint $table) {

            // Main Schema
            $table->bigIncrements('id');
            $table->unsignedBigInteger('post_id');
            $table->string('image_path');

            // Meta Data
            // This includes the 'created_at' and 'updated_at' columns
            $table->timestamps();

            // Keys
            $table->foreign('post_id')->references('id')->
                on('posts')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('posts_images');
    }
}