<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('users', function (Blueprint $table) {

            // Main Schema
            $table->bigIncrements('id');
            $table->string('email')->unique();
            $table->string('password')->nullable(); // Is nullable so external API's can be used to log in users.
            $table->string('firstname');
            $table->string('surname');

            // API Authentication
            $table->string('api_token');

            // Google Schema
            $table->string('provider_name')->nullable();
            $table->string('provider_id')->nullable();

            // Meta Data
            // This includes the 'created_at' and 'updated_at' columns
            $table->timestamps();
            $table->rememberToken();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('users');
    }
}
