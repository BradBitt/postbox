<?php

use App\PostImage;
use Illuminate\Database\Seeder;

class PostImagesTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        // Populates the table.
        factory(PostImage::class, 5)->create();
    }
}
