<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        // Creates a user that we can log in with.
        User::create([
            'firstname' => 'Bradley',
            'surname' => 'Tenuta',
            'email' => 'bradley@example.com',
            'password' => Hash::make('password'),
            'api_token' => Str::random(80),
        ]);

        // Creates a second test user
        User::create([
            'firstname' => 'John',
            'surname' => 'Doe',
            'email' => 'test@example.com',
            'password' => Hash::make('password'),
            'api_token' => Str::random(80),
        ]);

        // Populates the table.
        factory(User::class, 100)->create();

        // Creates many to many relationships by populating the pivot table.
        $this->populateSubscriberTables();
    }

    /**
     * This function populates the pivot table
     * of subscribers.
     */
    private function populateSubscriberTables() {

        // Populate the subscriber pivot table
        User::all()->each(function ($user) { 

            // Gets another random user that doesn't match the iterating user.
            $random_user = User::inRandomOrder()->first();
            while ($random_user == $user) {
                $random_user = User::inRandomOrder()->first();
            }
            
            // Then adds the iterating user as a subscriber to the random user
            $user->subscribedTo()->attach($random_user->id); 
        });
    }
}