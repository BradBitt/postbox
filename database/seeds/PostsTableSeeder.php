<?php

use App\Post;
use App\User;
use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        // Creates a post for the example user.
        Post::create([
            'title' => 'This is my test post title',
            'content' => 'So, I wasnt sure what to write here, but this text is just to show that my post has content.',
            'user_id' => User::findOrFail(1)->id,
        ]);

        // Populates the table.
        factory(Post::class, 25)->create();
    }
}
