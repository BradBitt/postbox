<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run() {

        // The seeders get called in this order:
        $this->call(UsersTableSeeder::class);

        $this->call(PostsTableSeeder::class);
        $this->call(PostImagesTableSeeder::class);

        $this->call(CommentsTableSeeder::class);
    }
}