<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable {

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'firstname', 'surname', 'password', 'provider_name', 'provider_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Gets a list of all the posts the user has.
     */
    public function posts() {
        return $this->hasMany('App\Post');
    }

    /**
     * Gets a list of all the comments the user has.
     */
    public function comments() {
        return $this->hasMany('App\Comment');
    }

    /**
     * Gets a list of all the users that are subscribed to the user.
     */
    public function subscribers() {
        // Including the name of the pivot table.
        return $this->belongsToMany('App\User', 'subscriptions', 'subscribed_to_user_id', 'subscriber_user_id');
    }

    /**
     * Gets a list of all the users that this user is subscribed to.
     */
    public function subscribedTo() {
        // Including the name of the pivot table.
        return $this->belongsToMany('App\User', 'subscriptions', 'subscriber_user_id', 'subscribed_to_user_id');
    }

    /**
     * Checks to see if the current user is subscribed to the second user.
     */
    public function isSubscribed($current_user, $second_user) {

        // Checks to see if the second user is inside the current users subscribed list.
        if ($current_user->subscribedTo->where('id', $second_user->id)->first()) {
            return true;
        }
        
        return false;
    }
}