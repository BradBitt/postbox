<?php

namespace App\Http\Controllers;

use App\Post;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AccountController extends Controller
{
    /**
     * Shows the user's profile.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function show()
    {

        // Gets all the posts that this user has created
        $posts = Post::where('user_id', Auth::user()->id)->orderBy('created_at', 'desc')->paginate(6);

        return view('auth.account', ['posts' => $posts]);
    }

    /**
     * This function updates the user settings.
     */
    public function apiUpdate(Request $request) {

        // Validates the request. Makes sure the user content is valid.
        $request->validate([
            'firstname' => 'required',
            'surname' => 'required',
            'user_id' => 'required',
            'api_token' => 'required',
        ]);

        // Checks the current user matches the user id by the api token.
        $user = User::findOrFail($request['user_id']);
        if ($user->api_token != $request['api_token']) {
            return;
        }

        // Updates the user
        $user->firstname = $request['firstname'];
        $user->surname = $request['surname'];
        $user->save();

        return $user;
    }
}