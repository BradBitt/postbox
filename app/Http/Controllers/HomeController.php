<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function show()
    {

        // Gets a number of posts to use for pagination
        // Orders them in order of when they were created.
        $posts = Post::orderBy('created_at', 'desc')->paginate(6);

        return view('auth.home', ['posts' => $posts]);
    }
}