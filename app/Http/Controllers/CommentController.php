<?php

namespace App\Http\Controllers;

use App\Comment;
use App\User;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    /**
     * This function is used by AJAX to store data without having to refresh the page.
     */
    public function apiStore(Request $request) {

        // Validates the request. Makes sure the content is valid.
        $request->validate([
            'user_id' => 'required',
            'post_id' => 'required',
            'content' => 'required',
            'api_token' => 'required',
        ]);

        // Checks the current user matches the user id by the api token.
        $user = User::findOrFail($request['user_id']);
        if ($user->api_token != $request['api_token']) {
            return;
        }

        // Creates the comment
        $comment = new Comment;
        $comment->user_id = $request['user_id'];
        $comment->post_id = $request['post_id'];
        $comment->content = $request['content'];
        $comment->save();

        return $comment;
    }

    /**
     * This function uses AJAX to update the provided comments contents.
     */
    public function apiUpdate(Request $request) {

        // Validates the request. Makes sure the content is valid.
        $request->validate([
            'comment_id' => 'required',
            'user_id' => 'required',
            'post_id' => 'required',
            'content' => 'required',
            'api_token' => 'required',
        ]);

        // Checks the current user matches the user id by the api token.
        $comment = Comment::findOrFail($request['comment_id']);
        if ($comment->user->api_token != $request['api_token']) {
            return;
        }

        // Updates the post
        $comment->content = $request['content'];
        $comment->save();

        return $comment;
    }
}