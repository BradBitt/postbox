<?php

namespace App\Http\Controllers;

use App\User;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Shows the correct user.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function show($id)
    {
        $user = User::findOrFail($id);

        // If the current user is trying to view view their account
        // then redirect to the account page.
        if ($user == Auth::user()) {
            return redirect('/account');
        }

        // Gets all the posts that this user has created
        $posts = Post::where('user_id', $id)->orderBy('created_at', 'desc')->paginate(6);

        return view('auth.user', ['user' => $user], ['posts' => $posts]);
    }

    /**
     * Subscribes the provided user to the current user.
     */
    public function subscribe(Request $request) {

        // Validates the request. Makes sure the request is valid.
        $request->validate([
            'user_id' => 'required',
            'subscribed_user_id' => 'required',
            'api_token' => 'required',
        ]);

        // Checks the current user matches the user id by the api token.
        $user = User::findOrFail($request['user_id']);
        if ($user->api_token != $request['api_token']) {
            return;
        }

        // Finds the user to subscribe to.
        $subscribe_to_user = User::findOrFail($request['subscribed_user_id']);

        // Subscribes the user.
        $user->subscribedTo()->attach($subscribe_to_user->id); 

        return $user;
    }

    /**
     * Unsubscribes the provided user from the current user.
     */
    public function unsubscribe(Request $request) {

        // Validates the request. Makes sure the request is valid.
        $request->validate([
            'user_id' => 'required',
            'subscribed_user_id' => 'required',
            'api_token' => 'required',
        ]);

        // Checks the current user matches the user id by the api token.
        $user = User::findOrFail($request['user_id']);
        if ($user->api_token != $request['api_token']) {
            return;
        }

        // Finds the user to subscribe to.
        $subscribe_to_user = User::findOrFail($request['subscribed_user_id']);

        // Subscribes the user.
        $user->subscribedTo()->detach($subscribe_to_user->id); 

        return $user;
    }
}