<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Socialite;
use Auth;
use Exception;

class SocialAuthGoogleController extends Controller
{
    /**
     * Redirect the user to the provider authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirect()
    {
        return Socialite::driver('google')->redirect();
    }

    /**
     * Obtain the user information from provider.
     *
     * @return \Illuminate\Http\Response
     */
    public function callback()
    {
        try {
            $user = Socialite::driver('google')->user();
        } catch (Exception $e) {
            return redirect()->route('login');
        }

        $existingUser = User::where('email', $user->getEmail())->first();

        if ($existingUser) {
            auth()->login($existingUser, true);
        } else {

            // Splits the name up into seperate words
            $names = explode(" ", $user->getName());
            $surname = $names[0];
            $firstname = $names[0];

            // if the length of the array is greater than 1.
            if (count($names) > 1) {

                $surname = array_pop($names);
            }

            // Creates a new user.
            $newUser = new User;
            $newUser->provider_name = 'google';
            $newUser->provider_id = $user->getId();
            $newUser->firstname = $firstname;
            $newUser->surname = $surname;
            $newUser->email = $user->getEmail();
            $newUser->api_token = Str::random(80);
            $newUser->save();

            auth()->login($newUser, true);
        }

        return redirect()->route('home');
    }
}