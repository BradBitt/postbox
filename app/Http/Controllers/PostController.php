<?php

namespace App\Http\Controllers;

use App\Post;
use App\PostImage;
use App\User;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * This function is used by AJAX to store data without having to refresh the page.
     */
    public function apiStore(Request $request) {

        // Validates the request. Makes sure the content is valid.
        $request->validate([
            'user_id' => 'required',
            'title' => 'required',
            'content' => 'required',
            'file' => 'sometimes|mimes:jpeg,jpg,png',
            'api_token' => 'required',
        ]);

        // Checks the current user matches the user id by the api token.
        $user = User::findOrFail($request['user_id']);
        if ($user->api_token != $request['api_token']) {
            return;
        }

        // Creates the post
        $post = new Post;
        $post->user_id = $request['user_id'];
        $post->title = $request['title'];
        $post->content = $request['content'];
        $post->save();

        // Only creates the post image if a file was provided.
        if ($request['file'] != null) {

            // Creates the image for the post and saves the image.
            $post_image = new PostImage;
            $post_image->post_id = $post->id;
            $post_image->image_path = $request->file->store('public/posts/' . $post->id);
            $post_image->save();
        }

        return $post;
    }

    /**
     * This function tates a request and updates the given post based on
     * the values within the request.
     */
    public function apiUpdate(Request $request) {

        // Validates the request. Makes sure the content is valid.
        $request->validate([
            'post_id' => 'required',
            'user_id' => 'required',
            'title' => 'required',
            'content' => 'required',
            'api_token' => 'required',
        ]);

        // Checks the current user matches the user id by the api token.
        $post = Post::findOrFail($request['post_id']);
        if ($post->user->api_token != $request['api_token']) {
            return;
        }

        // Updates the post
        $post->title = $request['title'];
        $post->content = $request['content'];
        $post->save();

        return $post;
    }

    /**
     * This function is called when the user wants to delete their post.
     */
    public function apiDelete(Request $request) {

        // Validates the request. Makes sure the content is valid.
        $request->validate([
            'post_id' => 'required',
            'user_id' => 'required',
            'api_token' => 'required',
        ]);

        // Checks the current user matches the user id by the api token.
        $post = Post::findOrFail($request['post_id']);
        if ($post->user->api_token != $request['api_token']) {
            return;
        }

        // Finds the post and deletes it.
        $post->delete();
    }
}