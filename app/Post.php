<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Post extends Model {

    /**
     * Gets the user that owns this post.
     */
    public function user() {
        return $this->belongsTo('App\User');
    }

    /**
     * Gets a list of all the comments the post has.
     */
    public function comments() {
        return $this->hasMany('App\Comment');
    }

    /**
     * Gets the image for this post.
     */
    public function image() {
        return $this->hasOne('App\PostImage');
    }

    /**
     * This function returns weather or not this post has an image.
     */
    public function hasImage() {

        // Checks to see if the post has an image.
        if ($this->image) {
            return true;
        }

        return false;
    }

    /**
     * This function is called when the model is deleted.
     * I am overwriting this function so that we can do other functionality before we delete the model.
     * Before a post is deleted, we make sure we delete the image linked to this post.
     */
    public function delete() {

        // If the post has an image, delete the image.
        if ($this->hasImage()) {
            Storage::delete($this->image->image_path);
        }

        // Calls the parent delete function.
        parent::delete();
    }
}
