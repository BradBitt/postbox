<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostImage extends Model {

    /**
     * Gets the post this image belongs to.
     */
    public function post() {
        return $this->belongsTo('App\Post');
    }
}