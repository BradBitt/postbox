<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model {

    /**
     * Gets the post that this comment relates to.
     */
    public function post() {
        return $this->belongsTo('App\Post');
    }

    /**
     * Gets the user that owns this comment.
     */
    public function user() {
        return $this->belongsTo('App\User');
    }
}