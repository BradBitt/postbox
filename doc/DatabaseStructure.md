- Users
    - firstName (String)
    - secondName (String)
    - email (string) (PK)
    - password
    - userId (Int) (PK)

- Posts
    - title (String)
    - content (String)
    - userId (Int) (FK)
    - postId (Int) (PK)
    - dateCreated (Date) (doesnt change if the user edits their comment)

Posts: Can display all posts in order of creation on homepage.
Posts: one-to-many

- Comments
    - userId (Int) (FK) (Owner of the comment)
    - postId (Int) (FK) (Post it belongs to)
    - commentId (Int) (PK)
    - content (String)
    - dateCreated (Date) (doesnt change if the user edits their comment)

Comments: one-to-many

- Comments_Posts
    - postId (Int) (PK)
    - commentId (Int) (PK)

Comments_Posts: one-to-many

- Users_Posts
    - postId (Int) (PK)
    - userId (Int) (PK)

Users_Posts: one-to-many

- Posts_Images
    - postId (Int) (PK)
    - image (String) (relative path, including filename)

Posts_Images: one-to-one

- Subscribers
    - subscriberUserId (userId)
    - subscribedToUserId (userId)

Subscribers: many-to-many