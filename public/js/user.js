/**
 * This function either subscirbes or unsubscribes the two users.
 * It also uses AJAX to update the DOM to show wheather the user is subscibed or not.
 * @param {*} subscribe_url 
 * @param {*} unsubscribe_url 
 * @param {*} provided_user_id 
 * @param {*} provided_subscribed_user_id 
 * @param {*} privided_api_token
 */
function toggleSubscribe(subscribe_url, unsubscribe_url, provided_user_id, provided_subscribed_user_id, privided_api_token) {

    // Gets the subsribe button and checks if its a subscirbe or unsubscribe button.
    var subscribe_button_text = $('#subscribe-button').text();

    if(subscribe_button_text == "Subscribe") {

        // Uses ajax to subscribe the user
        axios.post(subscribe_url, {
            user_id: provided_user_id,
            subscribed_user_id: provided_subscribed_user_id,
            api_token: privided_api_token,
        }).then(response => {
            
            // Changes the text of the subscribe button.
            $('#subscribe-button').text("UnSubscribe");

        }).catch(response => {
            console.log(response);
        });

    } else {

        // Uses ajax to unsubscribe the user
        axios.post(unsubscribe_url, {
            user_id: provided_user_id,
            subscribed_user_id: provided_subscribed_user_id,
            api_token: privided_api_token,
        }).then(response => {
            
            // Changes the text of the subscribe button.
            $('#subscribe-button').text("Subscribe");

        }).catch(response => {
            console.log(response);
        });
    }
}