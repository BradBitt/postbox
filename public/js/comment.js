/**
 * This function uses AJAX to create a comment and display it in the DOM.
 * I have not used Vue to use this, just JQuery and Axios.
 * @param {*} url 
 * @param {*} provided_post_id 
 * @param {*} provided_user_id 
 * @param {*} firstname 
 * @param {*} surname 
 * @param {*} old_number_of_comments 
 * @param {*} privided_api_token
 */
function makeNewComment(url, provided_post_id, provided_user_id, firstname, surname, old_number_of_comments, privided_api_token) {

    // Gets the comment content container.
    var content = $("#make-comment-container-" + provided_post_id + " input").val();
    var new_number_of_comments = parseInt(old_number_of_comments) + 1;

    // Uses ajax to add a new comment
    axios.post(url, {
        content: content,
        user_id: provided_user_id,
        post_id: provided_post_id,
        api_token: privided_api_token,
    }).then(response => {

        // Adds the comment to the UI.
        $("#" + provided_post_id + " .comment-outer-container").append(
            "<div class=\"comment-container\"><p class=\"comment-user\">" +
            firstname + " " + surname + ":" + 
            "</p><p class=\"comment-content\">" + content + "</p></div>");

        // Resets the comment text area
        $("#make-comment-container-" + provided_post_id + " input").val("");

        // Makes sure the comment container height updates
        changeCommentsContainer(provided_post_id);

        // Updates the number of comments in the toggle comments button.
        $("#" + provided_post_id + " .btn-lg").text("Comments (" + new_number_of_comments + ")");

        // TODO: Edit comment icon does not appear when a comment is created, only when page is refreshed.

    }).catch(response => {
        console.log(response);
    });
}

/**
 * This function updates the contents of a given comment. This uses AJAX to
 * update the comment within the database and then also updates the UI live
 * using JQuery.
 * @param {*} url 
 * @param {*} provided_post_id 
 * @param {*} provided_user_id 
 * @param {*} provided_comment_id 
 * @param {*} privided_api_token
 */
function editComment(url, provided_post_id, provided_user_id, provided_comment_id, privided_api_token) {

    // Gets the comments content
    var content = $("#edit-comment-content-" + provided_comment_id).val()

    // Uses ajax to update the comment
    axios.post(url, {
        comment_id: provided_comment_id,
        post_id: provided_post_id,
        content: content,
        user_id: provided_user_id,
        api_token: privided_api_token,
    }).then(response => {

        // Updates the UI
        $("#comment-container-" + provided_comment_id + " .comment-content").text(content);
        
        // Updates the input values with the new content.
        $("#edit-comment-content-" + provided_comment_id).val(content);

    }).catch(response => {
        console.log(response);
    });
}