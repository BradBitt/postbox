/**
 * This function creates a new post 
 * @param {*} url 
 * @param {*} provided_user_id 
 * @param {*} firstname 
 * @param {*} surname 
 * @param {*} privided_api_token
 */
function makeNewPost(url, provided_user_id, privided_api_token) {

    // Creates a form data object to add all the parameters to.
    var formData = new FormData();
    formData.append("user_id", provided_user_id);

    // gets the title of the new post
    var title = $('#create_post_modal #new-post-title').val();
    formData.append("title", title);

    // Gets the content of the new post
    var content = $('#create_post_modal #new-post-content').val();
    formData.append("content", content);

    // Adds the api token.
    formData.append("api_token", privided_api_token);

    // Gets the image if there is one.
    // Only gets the first file.
    if ($('#new-post-file').get(0).files.length != 0) {
        formData.append("file", $('#new-post-file').prop('files')[0]);
    }

    // Uses ajax to add a new post
    axios.post(url, formData, {
        headers: {
            'Content-Type': 'multipart/form-data'
        },
    }).then(response => {
        
        // Clears the text in the input areas.
        $('#create_post_modal #new-post-title').val("");
        $('#create_post_modal #new-post-content').val("");

    }).catch(response => {
        console.log(response);
    });
}

/**
 * Updates the post provided with the new content and title.
 * This then uses JQuery to update the DOM to display the changes.
 * @param {*} url 
 * @param {*} provided_user_id 
 * @param {*} provided_post_id 
 * @param {*} privided_api_token
 */
function editPost(url, provided_user_id, provided_post_id, privided_api_token) {

    // Gets the posts title
    var title = $("#edit-post-title-" + provided_post_id).val()

    // Gets the posts content
    var content = $("#edit-post-content-" + provided_post_id).val()

    // Uses ajax to update the post
    axios.post(url, {
        post_id: provided_post_id,
        content: content,
        user_id: provided_user_id,
        title: title,
        api_token: privided_api_token,
    }).then(response => {

        // Updates the UI
        $("#" + provided_post_id + " .post-title").text(title);
        $("#" + provided_post_id + " .post-content").text(content);
        
        // Clears the text in the input areas.
        $("#edit-post-title-" + provided_post_id).val(title);
        $("#edit-post-content-" + provided_post_id).val(content);

    }).catch(response => {
        console.log(response);
    });
}

/**
 * Deletes the desired post.
 * @param {*} url 
 * @param {*} provided_post_id 
 * @param {*} privided_api_token
 */
function deletePost(url, provided_post_id, privided_api_token) {

    // Uses ajax to delete the post
    axios.post(url, {
        post_id: provided_post_id,
        api_token: privided_api_token,
    }).then(response => {

        // Updates the UI by deleting the post.
        $("#" + provided_post_id).fadeOut(300, function() { $(this).remove(); });

    }).catch(response => {
        console.log(response);
    });
}