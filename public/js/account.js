/**
 * This function uses AJAX to update the users information.
 * It then uses JQuery to update the information being displayed in the webpage.
 * @param {*} url 
 * @param {*} provided_user_id
 * @param {*} privided_api_token
 */
function updateAccount(url, provided_user_id, privided_api_token) {

    // Gets the firstname content
    var new_firstname = $('#edit-account-firstname').val();

    // Gets the surname content
    var new_surname = $('#edit-account-surname').val();

    // Uses ajax to update the post
    axios.post(url, {
        firstname: new_firstname,
        surname: new_surname,
        user_id: provided_user_id,
        api_token: privided_api_token,
    }).then(response => {

        // Updates the UI
        $("#firstname-surname-container").text("Name: " + new_firstname + " " + new_surname);
        
        // Updates the input values with the new content.
        $('#edit-account-firstname').val(new_firstname);
        $('#edit-account-surname').val(new_surname);

    }).catch(response => {
        console.log(response);
    });
}