/**
 * This function increases or decreases the height of a posts comment container.
 * It knows which comment container to open/close based on the id provided.
 * The function basically animates an opening closing animation of a comment container.
 * @param {*} id 
 */
function changeCommentsContainer(id) {

    // Gets the comment container object
    var comment_container = $("#" + id + " .comment-outer-container");
    var animate_time = 500;

    // Gets the current height.
    var curHeight = comment_container.height();

    // Gets the height for 'auto' then sets the height back to current height.
    var autoHeight = comment_container.css('height', 'auto').height();
    comment_container.height(curHeight);

    // if the height is 0, then extend the height of the comment container.
    if (comment_container.height() != autoHeight) {

        // Animates the height to auto in miniseconds.
        comment_container.stop().animate({ height: autoHeight }, animate_time);
    } else {

        // Animates the height to 0, effectivly closing the comment container.
        comment_container.stop().animate({ height: 0 }, animate_time);
    }
}