<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

// The login route and controller are already defined by default.
// If the user is logged in already, then they cannot access the login page and are
// redirected to the home page.
Route::get('/', function () { 
    return redirect()->route('login'); 
});

// Route to the home page.
Route::get('/home', 'HomeController@show')->name('home')->middleware('auth');

// Route to show a user profile.
Route::get('/account', 'AccountController@show')->name('account')->middleware('auth');

// Route to show another user
Route::get('/users/{id}', 'UserController@show')->name('users.show')->middleware('auth');

// Google Routes
Route::get('/redirect', 'SocialAuthGoogleController@redirect')->name('google.redirect');
Route::get('/callback', 'SocialAuthGoogleController@callback')->name('google.callback');