<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Routes for AJAX comments
Route::post('/comments/store', 'CommentController@apiStore')->name('api.comments.store');
Route::post('/comments/update', 'CommentController@apiUpdate')->name('api.comments.update');

// Routes for AJAX posts
Route::post('/posts/store', 'PostController@apiStore')->name('api.posts.store');
Route::post('/posts/update', 'PostController@apiUpdate')->name('api.posts.update');
Route::post('/posts/delete', 'PostController@apiDelete')->name('api.posts.delete');

// Routes for AJAX subscribing
Route::post('/users/subscribe', 'UserController@subscribe')->name('api.users.subscribe');
Route::post('/users/unsubscribe', 'UserController@unsubscribe')->name('api.users.unsubscribe');

// Routes for AJAX acount
Route::post('/account/update', 'AccountController@apiUpdate')->name('api.account.update');